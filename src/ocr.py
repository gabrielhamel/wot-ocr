import cv2
import pytesseract
import re

def get_timer(frame):
    width = frame.shape[1]

    y1 = 6
    y2 = 23
    x1 = int(0.996875 * width - 51.0)
    x2 = int(width - 14)

    cropped_frame = frame[y1:y2, x1:x2]
    timer_gray = cv2.cvtColor(cropped_frame, cv2.COLOR_BGR2GRAY)
    _, thresh1 = cv2.threshold(timer_gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
    text = pytesseract.image_to_string(thresh1, lang='eng', config='--psm 6')
    text = text.strip()
    text = re.sub(r'[^0-9]', '', text)
    if len(text) != 4:
        raise Exception('Unable to read timer')

    minutes = int(text[:2])
    seconds = int(text[2:])

    return minutes * 60 + seconds
