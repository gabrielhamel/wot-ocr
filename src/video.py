import cv2

def get_frame_at(video_path, seconds):
    video = cv2.VideoCapture(video_path)
    fps = video.get(cv2.CAP_PROP_FPS)
    frame_no = 0
    while video.isOpened():
        _, curr_frame = video.read()
        if frame_no == seconds * fps:
            return curr_frame
        frame_no += 1
    video.release()
    raise Exception('Unable to retrieve the frame')
